import Ember from 'ember';

export default Ember.Route.extend({
  model: function () {
    return this.store.find('user',{current: true});
  },

  setupController: function (controller, model) {
    controller.set('model', model.get('content.firstObject'));
  }
});
