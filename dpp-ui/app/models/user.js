import DS from 'ember-data';

export default DS.Model.extend({
  username: DS.attr('string'),
  updatedAt: DS.attr('date'),
  createdAt: DS.attr('date'),
  email: DS.attr('string')
});
