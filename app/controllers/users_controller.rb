class UsersController < ApplicationController
  prepend_before_filter :current_user
  before_filter :authenticate_user!
  respond_to :json

  def index
    if params[:current] == "true"
      @users = [current_user]
      render json: @users
    else
      @users = User.all
      render json: @users
    end
  end

  def show
    @user = User.find params[:id]
    render json: @user
  end

end
