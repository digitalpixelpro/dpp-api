class UserSerializer < ActiveModel::Serializer
  embed :ids, include: true

  attributes :id, :username, :email, :sign_in_count, :created_at, :updated_at
end
